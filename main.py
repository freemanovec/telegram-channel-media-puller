from telethon.sync import TelegramClient
from telethon.tl.functions.messages import SendVoteRequest
from telethon.tl.types import MessageMediaDocument, MessageMediaPhoto
from dotenv import load_dotenv
from os import getenv, makedirs, path
from sys import argv
import csv, sys
import datetime

def progress_callback(received, total):
    percentage = int(round(received / total * 100))
    print(f"  {percentage}%")

def main():
    load_dotenv()
    api_id = getenv('API_ID')
    api_hash = getenv('API_HASH')

    if len(argv) < 2:
        print("Pass channel names (without @) as arguments")
        return
    channel_names = [str(i) for i in argv[1:]]

    with TelegramClient("media_puller", api_id, api_hash) as client:
        for channel_name in channel_names:
            directory = f"out/{channel_name}"
            if not path.isdir(directory):
                makedirs(directory)
            for message in client.iter_messages(channel_name, reverse = False):
                if message.media is not None:
                    media = message.media
                    if isinstance(media, MessageMediaDocument) or isinstance(media, MessageMediaPhoto) and message.file is not None:
                        filename = message.file.name
                        if filename is None:
                            filename = f"{message.file.id}{message.file.ext}"
                        full_path = f"{directory}/{filename}"
                        if path.isfile(full_path):
                            print(f"Skipping {filename}")
                        else:
                            print(f"Downloading {filename}")
                            client.download_media(media, file=full_path, progress_callback=progress_callback)


if __name__ == "__main__":
    main()
