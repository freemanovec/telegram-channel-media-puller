FROM ubuntu:20.04

VOLUME [ "/out" ]
WORKDIR /
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y \
    python3 \
    python3-pip
ADD requirements.txt /
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r ./requirements.txt
ADD main.py /
ENV API_ID=""
ENV API_HASH=""
ENTRYPOINT [ "python3", "-u", "/main.py" ]
